
# ASX stocks data analysis

### Key files

    main_analysis_JL_mod.pdf         The final markdown report - read this.
    R_codes/main_analysis_JL.Rmd     The R markdown file, including codes and markdown report.

In this project, we go through how to explore and analyse ASX stocks data assuming zero prior knowledge on the data. The aim is to identify trading strategies to maximise profit based on single-day gains, a.k.a. 1-day future close returns.

Featuring:

* Data cleaning and sanity checks
* Exploratory Data Analysis
* Time-series (Autoregression)
* Forecasting

NOTE: Why the small-cap stocks strategy seems too good to be true? BECAUSE only close price was used - you can never achieve that with the bid-ask spread in the actual market

-------------------------

The first two pages of the report are embeded below. [Click here to download the full report](https://bitbucket.org/jtjli/asx_stocks_demo/raw/6b19ba64c0eea75a604568a3ebbd5dabef8748cd/main_analysis_JL_mod.pdf)

![Page 1](https://www.dropbox.com/s/bxaee5q16anp8cb/report_Page_1.jpg?dl=1)
![Page 2](https://www.dropbox.com/s/epbkuf4au351v1u/report_Page_2.jpg?dl=1)

[Click here to download the full report](https://bitbucket.org/jtjli/asx_stocks_demo/raw/6b19ba64c0eea75a604568a3ebbd5dabef8748cd/main_analysis_JL_mod.pdf)

